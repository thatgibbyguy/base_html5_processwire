<ul id='breadcrumb'><?php

	// Create breadcrumb navigation by cycling through the current $page's
	// parents in order, linking to each:

	foreach($page->parents as $parent) {
		echo "<li><a href='{$parent->url}'>{$parent->title}</a> &gt; </li>";
	}

?></ul>