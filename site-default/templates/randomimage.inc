<?php

	// Grab a random image from the homepage and display it.
	// Note that $homepage was loaded above where we generated the top navigation.

	if(count($homepage->images)) {
		$image = $homepage->images->getRandom(); 
		$thumb = $image->size(232, 176); 	
		echo "<a href='{$image->url}'><img id='photo' src='{$thumb->url}' alt='{$thumb->description}' width='{$thumb->width}' height='{$thumb->height}' /></a>";
	}

?>