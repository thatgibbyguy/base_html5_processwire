<html>
    <head>
        <title>Earth</title>
    </head>
    <body>

      <header>
        <nav>
          <?php $treeMenu = $modules->get("MarkupSimpleNavigation");?> // load the module
          <?php echo $treeMenu->render(); ?>// render default menu 
        </nav>
      </header>

       <h1>Earth</h1>
       <h2>Type: Happy planet, Age: Millions of years</h2>
       <p>Earth (or the Earth) is the third planet from the Sun, and the densest and fifth-largest 
       of the eight planets in the Solar System. It is also the largest of the Solar System's  
       four terrestrial planets. It is sometimes referred to as the World, the Blue Planet, 
       or by its Latin name, Terra.</p>

       <h1><?php echo $page->planet_age; ?></h1>
       <h2><?php echo $page->planet_type; ?></h2>
       <p><?php echo $page->planet_summary; ?></p>

    </body>
</html>