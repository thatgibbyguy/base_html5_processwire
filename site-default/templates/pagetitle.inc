<h1 id='title'><?php 

	// The statement below asks for the page's headline or title. 
	// Separating multiple fields with a pipe "|" returns the first
	// one that has a value. So in this case, we print the headline
	// field if it's there, otherwise we print the title. 
	
	echo $page->get("headline|title"); 

?></h1>