<?php

/**
 * Demo site header include file (HTML5)
 *
 * Note that this file has nothing to do with ProcessWire. We just split our common 
 * header and footer markup into separate files (head.inc and foot.inc) like this, 
 * since it was common to all of our templates. 
 *
 */

?>
<!DOCTYPE html>

<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />

	<title><?php echo $page->get("headline|title"); ?></title>

	<meta name="description" content="<?php echo $page->summary; ?>" />

	<meta name="generator" content="ProcssWire <?php echo $config->version; ?>" />

	<link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates?>css/styles.css" />

	<script type="text/javascript" src="<?php echo $config->urls->templates?>js/libs/modernizr-2.6.2.min.js"></script>

	<!--
	This website is powered by ProcessWire CMF/CMS.
	ProcessWire is a free open source content management framework licensed under the GNU GPL.
	ProcessWire is Copyright 2012 by Ryan Cramer / Ryan Cramer Design, LLC.
	Learn more about ProcessWire at: http://processwire.com
	-->

</head>
<body>

	<section>
		<header>
			<h1>
				<a href='<?php echo $config->urls->root; ?>' class="brand">
				</a>
			</h1>
			<ul><?php
			
				// Create the top navigation list by listing the children of the homepage. 
				// If the section we are in is the current (identified by $page->rootParent)
				// then note it with <a class='on'> so we can style it differently in our CSS. 
				// In this case we also want the homepage to be part of our top navigation, 
				// so we prepend it to the pages we cycle through:
				
				$homepage = $pages->get("/"); 
				$children = $homepage->children;
				$children->prepend($homepage); 
		
				foreach($children as $child) {
					$class = $child === $page->rootParent ? " class='on'" : '';
					echo "<li><a$class href='{$child->url}'>{$child->title}</a></li>";
				}

			?></ul>
		</header>
	</section>
			
