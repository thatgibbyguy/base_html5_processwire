<div id="sidebar">

				<?php

				// Output subnavigation
				// 
				// Below we check to see that we're not on the homepage, and that 
				// there are at least one or more pages in this section.
				// 
				// Note $page->rootParent is always the top level section the page is in, 
				// or to word differently: the first parent page that isn't the homepage.

				if($page->path != '/' && $page->rootParent->numChildren > 0) { 

					// We have determined that we're not on the homepage
					// and that this section has child pages, so make navigation: 

					echo "<ul id='subnav' class='nav'>";

					foreach($page->rootParent->children as $child) {
						$class = $page === $child ? " class='on'" : '';
						echo "<li><a$class href='{$child->url}'>{$child->title}</a></li>";	
					}

					echo "</ul>";
				}

				?>

				<div class='sidebar_item'>

					<?php

					// if the current page has a populated 'sidebar' field, then print it,
					// otherwise print the sidebar from the homepage
					
					if($page->sidebar) echo $page->sidebar; 
						else echo $homepage->sidebar; 
					?>


				</div>